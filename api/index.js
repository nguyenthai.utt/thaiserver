
/**
 * Created by trungquandev.com's author on 02/07/2019.
 * server.js
 */
const ObserveClass = require("./Observe");

// Init Observe object
var Observe = new ObserveClass();
const readLastLines = require("read-last-lines");

/**
 * Watching folder
 */
// Define folder to watching, in real project, you should put it in file config or env
let targetFolder = "../laravel-example/storage/logs";

// Listen event new file has been added
Observe.on("new-file-has-been-added", (logData) => {
  // In this step, you can do anything you want, like to push alert message to chatwork, slack...vv
  // I just print error message to console
  console.log(test);
  // test.
});

// Start watching folder...
//  Observe.watchFolder(targetFolder);

const fsF = require('fs');

const fs2 = require('fs');
const path = require('path');

const http = require('http');
const sockjs = require('sockjs');

const wss = sockjs.createServer();
const clients = new Map();


wss.on('connection', (ws) => {
  console.log("connected");
  const id = uuidv4();
  const color = Math.floor(Math.random() * 360);
  const metadata = { id, color };


  clients.set(ws, metadata);

  /**
 * Watching file
 */
  // Define file to watching, in real project, you should put it in file config or env
  var targetFile = "C:/logs/eBunsyo/ebunsyo-log";
  var targetFolderLog = "C:/logs/";
  var targetFolderLogTomcat = "C:/logs/eBunsyo/";

  // Start watching file...
  Observe.watchFile(targetFile);

  ws.on('data', (messageAsString) => {
    const message = JSON.parse(messageAsString);
    const metadata = clients.get(ws);

    if (message && message.folder && message.isFolder) {
      const testFolder = (message.folder.trim() == 'TomcatLog' ? targetFolderLogTomcat : (targetFolderLog + message.folder));
      let listFile = []
      fsF.readdir(testFolder, (err, files) => {
        files.forEach((file, index) => {
          listFile.push(file)
          if (index == files.length - 1) {
            [...clients.keys()].forEach((client) => {
              client.write(JSON.stringify({
                isFolder: false,
                isFile: true,
                data: listFile,
              }));
            });
          }
        });
      });
    }
    if (message && message.isFirst) {
      fs2.readdir(targetFolderLog, { withFileTypes: true }, (error, files) => {
        const directoriesInDIrectory = files
          .filter((item) => item.isDirectory())
          .map((item) => item.name);
        [...clients.keys()].forEach((client) => {
          client.write(JSON.stringify({
            isFolder: true,
            data: directoriesInDIrectory,
          }));
        });
        // console.log(directoriesInDIrectory);
      });
    }

    if (message && message.file) {
      const targetFile1 = (message.folder.trim() == 'TomcatLog' ? targetFolderLogTomcat : (targetFolderLog + message.folder.trim() + '/')) + message.file.trim();

      readLastLines.read(targetFile1, message.maxline)
        .then((lines) => {
          [...clients.keys()].forEach((client) => {
            client.write(JSON.stringify({
              isLog: true,
              data: lines,
            }));
          });
        });

      fs.watchFile(targetFile1, { interval: 1000 }, (curr, prev) => {
        // Read content of new file
        // let updateContent = readLastLines.read(targetFile, 10);
        readLastLines.read(targetFile1, 100)
          .then((lines) => {
            [...clients.keys()].forEach((client) => {
              client.write(JSON.stringify({
                isLog: true,
                data: lines,
              }));
            });
          });
      });

      // Start watching file...
      Observe.watchFile(targetFile1);
    }

  });

  ws.on("close", () => {
    clients.delete(ws);
  });
});

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

const server = http.createServer();
wss.installHandlers(server, { prefix: '/ws' });
server.listen(7071, '0.0.0.0');

// console.log("wss up 123");

const fs = require('fs');

const buttonPressesLogFile = 'C:/logs/eBunsyo/ebunsyo-log';

console.log(`Watching for file changes on ${buttonPressesLogFile}`);

// fs.watch(buttonPressesLogFile, (event, filename) => {
//   if (filename) {
//     console.log(event);
//   }
// });
const fsExtra = require("fs-extra");


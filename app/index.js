
(async function () {

    const ws = await connectToServer(1000);

    //Click Menu
    document.getElementById('sidebarnav').onclick = (evt) => {
        $('#viewlog .content').html('')
        $('#viewlog .content').hide();
        $('#viewlog .loadding').show();
        $('#refesh').hide();
        const messageBody = { folder: evt.path[0].textContent, isFolder: true };
        ws.send(JSON.stringify(messageBody));
    };

    //Click Menu
    document.getElementById('select-file').onchange = (evt) => {
        $('#viewlog .content').html('')
        $('#viewlog .content').hide();
        $('#viewlog .loadding').show();
        $('#refesh').hide();
        const messageBody = { folder: $('.sidebar-item.active').text(), file: evt.target.value, isFile: true, maxline: $('#select-maxline').val() };
        ws.send(JSON.stringify(messageBody));
    };

    //Click Menu
    document.getElementById('refesh').onclick = (evt) => {
        ws.lastLog = {};
        ws.listLog = new Array();
        ws.count = 0;
        $('#viewlog .content').html('')
        $('#viewlog .content').hide();
        $('#viewlog .loadding').show();
        $('#refesh').hide();
        const messageBody = { folder: $('.sidebar-item.active').text(), file: $('#select-file').val(), isFile: true, maxline: $('#select-maxline').val() };
        ws.send(JSON.stringify(messageBody));
    };

    setTimeout(() => {
        const messageBody = { maxline: 50, isFirst: true };
        ws.send(JSON.stringify(messageBody));
    }, 100)

    ws.lastLog = {};
    ws.listLog = new Array();
    ws.count = 0;


    ws.onmessage = (webSocketMessage) => {
        const response = JSON.parse(webSocketMessage.data);
        // List Menu
        console.log(response);
        if (response.isFolder) {
            $('#sidebarnav').html('')
            $('#sidebarnav').append('<li class="sidebar-item" onclick="menuClick(this)"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="#" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">TomcatLog</span></a></li>')
            response.data.forEach(element => {
                $('#sidebarnav').append('<li class="sidebar-item" onclick="menuClick(this)"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="#" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">' + element + '</span></a></li>')
            });
            ws.count = 0;
        }

        if (response.isFile) {
            let select = $('#select-file').html('')
            // let select = document.createElement('select');
            // select.id = "select-file"
            select.append(new Option())
            response.data.forEach(element => {
                select.append(new Option(element, element))
            });
            ws.count = 0;
        }

        if (response.isConsole) {
            $('#viewlog .content').html('')
            $('#viewlog .content').append(response.data)
            $('#viewlog .loadding').hide();
            $('#refesh').hide();
            $('#viewlog .content').show();
        }

        if (response.isLog) {
            const newData = response.data.split('\n')
            let checkId = false;
            if ($('.sidebar-item.active').text().trim() == 'eBunsyo' || $('.sidebar-item.active').text().trim() == 'eKessai') {
                for (let i = 0; i < newData.length; i++) {
                    if (newData[i] == '') continue
                    const log = newData[i].split(' ');
                    const currentLog = (log[0] + ' ' + log[1]).split(',');
                    // if (ws.count == 50) {
                    //     debugger
                    // }
                    if (!checkId && ws.lastLog.time && new Date(ws.lastLog.time).getTime() > new Date(currentLog[0]).getTime()) {
                        continue
                    }
                    if (!checkId & ws.lastLog.time == currentLog[0] && ws.lastLog.key && ws.lastLog.key >= currentLog[1]) {
                        continue
                    }
                    checkId = true
                    const itemArr = newData[i].split(' - ');
                    const itemArr2 = itemArr[0].split(' ');
                    let span = document.createElement('span');

                    if (itemArr2[2] == 'DEBUG' || itemArr2[2] == 'INFO' || itemArr2[2] == 'TRACE') {
                        span.setAttribute('class', 'icon icon-green')
                    } else if (itemArr2[2] == 'WARN') {
                        span.setAttribute('class', 'icon icon-yellow')
                    } else if (itemArr2[2] == 'ERROR') {
                        span.setAttribute('class', 'icon icon-red')
                    } else {
                        span.setAttribute('class', 'icon icon-white')
                    }
                    span.append(itemArr2[2])
                    let div = document.createElement('div');
                    div.setAttribute('class', 'item-log')
                    div.append(ws.count);
                    let span2 = document.createElement('span');
                    span2.setAttribute('class', 'color-green')
                    span2.append(', ' + itemArr2[0] + ' ' + itemArr2[1])
                    div.append(span2);
                    div.append(span)
                    div.append(' - ' + itemArr[1])
                    $('#viewlog .content').append(div)
                    ws.count++;
                }

                if (ws.count > 100000) {
                    for (let i = 0; i < ws.count - 100000; i++) {
                        ws.count--;
                        document.querySelector('.item-log').remove();
                    }
                }

                if (checkId) {
                    let hr = document.createElement('hr');
                    $('#viewlog .content').append(hr)
                }
                const lastLogStr = newData[newData.length - 2].split(' ');
                const idLastLog = (lastLogStr[0] + ' ' + lastLogStr[1]).split(',');
                ws.lastLog = {
                    time: idLastLog[0],
                    key: idLastLog[1],
                }
            } else {
                for (let i = 0; i < newData.length; i++) {
                    ws.count++;
                    let div = document.createElement('div');
                    div.setAttribute('class', 'item-log')
                    div.setAttribute('class', 'item-log-space')
                    div.append(ws.count);
                    div.append(',');
                    div.append(newData[i]);
                    $('#viewlog .content').append(div)
                }
            }


            $('#viewlog .loadding').hide();
            $('#viewlog .content').show();
            $('#refesh').show();
        }





    };

    async function connectToServer() {
        const ws = new SockJS('http://localhost:7071/ws');
        return new Promise((resolve, reject) => {
            const timer = setInterval(() => {
                console.log('okkk');
                if (ws.readyState === 1) {
                    clearInterval(timer);
                    resolve(ws);
                }
            }, 10);
        });
    }

    function getOrCreateCursorFor(messageBody) {
        // console.log(messageBody);
        const sender = messageBody.sender;
        const existing = document.querySelector(`[data-sender='${sender}']`);
        if (existing) {
            return existing;
        }

        const template = document.getElementById('cursor');
        const cursor = template.content.firstElementChild.cloneNode(true);
        const svgPath = cursor.getElementsByTagName('path')[0];

        cursor.setAttribute("data-sender", sender);
        svgPath.setAttribute('fill', `hsl(${messageBody.color}, 50%, 50%)`);
        document.body.appendChild(cursor);

        return cursor;
    }

})();


// A $( document ).ready() block.
$(document).ready(function () {

});

